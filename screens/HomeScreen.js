import * as WebBrowser from 'expo-web-browser';
import * as React from 'react';
import {Image, Platform, StyleSheet, Text, TouchableOpacity, View} from 'react-native';
import {ScrollView} from 'react-native-gesture-handler';

import AppClient from "../api/client/app-client";

const client = new AppClient();

import {MonoText} from '../components/StyledText';
import type {ProductType} from "../product/ProductType";
import {createProductCustomisationDefaults} from "../product/Product";
import ProductCustomiser from "../components/product/ProductCustomiser";

type HomeScreenProps = {};

type HomeScreenState = {
    product: ProductType | null
};

export default class HomeScreen extends React.Component<HomeScreenProps, HomeScreenState>
{

    navigationOptions = {
        header: null,
    };

    constructor(props)
    {
        super(props);

        this.state = {
            product: null
        }
    }

    loadProduct(id: number)
    {
        client.get(`/product/${id}.json`).then((product: ProductType) => {
            this.setState({
                product: product
            })
        })
    }

    componentDidMount()
    {
        this.loadProduct(1);
    }

    render()
    {
        return (
            <View style={styles.container}>

                <ScrollView style={styles.container} contentContainerStyle={styles.contentContainer}>
                    <View style={styles.productContainer}>

                        {this.state.product ? <ProductCustomiser product={this.state.product} /> : null}

                    </View>
                </ScrollView>

            </View>
        );
    }

}

const styles = StyleSheet.create({
    container: {
        flex: 1,
        backgroundColor: '#fff',
    },
    productContainer: {
        alignItems: 'center',
        marginTop: 10,
        marginBottom: 20,
    },
    contentContainer: {
        paddingTop: 30,
    }
});
