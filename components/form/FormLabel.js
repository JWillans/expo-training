import React from 'react';

import {Text, View} from "react-native";

type FormLabelProps = {
    label: string
}

export default class FormLabel extends React.Component<FormLabelProps>
{

    render()
    {
        return (
            <View style={styles.container}>
                <Text style={styles.text}>{this.props.label}</Text>
            </View>
        )
    }

}

const styles = {
    text: {
        fontWeight: 'bold',
        fontSize: 20
    },
    container: {
        margin: 10
    }

}