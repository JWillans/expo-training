export type FormChoiceOptionType = {
    value: any,
    label: string
}

export type FormOptionsType = {
    label: string,
    choices: {value: any, label: string},
    [string]: any
}

export type FormTypeProps = {
    id: string,
    options: FormOptionsType,
    value: string | number | boolean | null,
    onChange: (value: any) => void
}

export const formTypeMap = {
    choice: require('./ChoiceFormType'),
    checkbox: require('./CheckboxFormType')
}