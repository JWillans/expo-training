import React from 'react';
import type {FormChoiceOptionType, FormTypeProps} from "./FormType";
import {View, Button, Text} from "react-native";
import FormLabel from "./FormLabel";

import Colours from '../../constants/Colors';

export default class ChoiceFormType extends React.Component<FormTypeProps>
{

    render()
    {
        const choices = this.props.options.choices || [];

        return (

            <View style={styles.container}>

                { this.props.options.label ? <FormLabel label={this.props.options.label} /> : null }

                {
                    choices.map((choice: FormChoiceOptionType, index: number) => {
                        const active = choice.value === this.props.value;
                        const color = active ? Colours.selected : Colours.unselected;
                        return (
                            <View style={styles.choiceContainer} key={choice.label}>
                                <Button color={color} onPress={this.createOnPress(index)} title={choice.label} />
                            </View>
                        )
                    })
                }

            </View>
        )
    }

    createOnPress(choiceIndex: number)
    {
        return () => {
            const choice = this.props.options.choices[choiceIndex];
            this.props.onChange(choice.value);
        }
    }

}

const styles = {
    container: {
        marginBottom: 10
    },
    choiceContainer: {
        marginBottom: 4
    }
}