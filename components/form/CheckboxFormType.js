import React from 'react';
import type {FormTypeProps} from "./FormType";
import {View, Button, Text} from "react-native";
import FormLabel from "./FormLabel";
import Colours from '../../constants/Colors';

export default class CheckboxFormType extends React.Component<FormTypeProps>
{

    render()
    {
        return (
            <View style={styles.container}>

                { this.props.options.label ? <FormLabel label={this.props.options.label} /> : null }

                <View style={styles.switchContainer}>
                    <View style={{flex: 0.5}}>
                        <Button
                            title="No"
                            onPress={this.createOnPress(false)}
                            color={!this.props.value ? Colours.selected : Colours.unselected}
                        />
                    </View>
                    <View style={{flex: 0.5}}>
                        <Button
                            title="Yes"
                            onPress={this.createOnPress(true)}
                            color={this.props.value ? Colours.selected : Colours.unselected}
                        />
                    </View>
                </View>

            </View>
        )
    }

    createOnPress(value: boolean): () => void
    {
        return () => {
            this.props.onChange(value);
        }
    }

}

const styles = {
    container: {
        marginBottom: 10
    },
    switchContainer: {
        flexDirection: "row"
    }
}