import React from 'react';
import type {CustomisationOptionsType, ImageType, ProductType} from "../../product/ProductType";
import {resolvePreviewImage, resolveProductPreviewImages} from "../../product/Product";
import {Image, View, StyleSheet} from "react-native";

type ProductPreviewProps = {
    product: ProductType,
    options: CustomisationOptionsType,
    scale: number,
    size: [number, number]
}

export default class ProductPreview extends React.Component<ProductPreviewProps>
{

    static defaultProps = {
        scale: 1,
        size: [823, 550]
    }

    render()
    {
        const previewImages = resolveProductPreviewImages(this.props.product, this.props.options);

        const containerStyle = {
            ...styles.container,
            position: 'relative',
            width: this.props.size[0] * this.props.scale,
            height: this.props.size[1] * this.props.scale
        }

        return (
            <View style={containerStyle}>
                {
                    previewImages.map((image: ImageType) =>
                        <Image
                            key={image.ref}
                            source={resolvePreviewImage(image.ref)}
                            style={{
                                position: 'absolute',
                                left: image.offset[0] * this.props.scale,
                                top: image.offset[1] * this.props.scale,
                                width: image.size[0] * this.props.scale,
                                height: image.size[1] * this.props.scale
                            }}
                        />
                    )
                }
            </View>
        )
    }

}

const styles = StyleSheet.create({
    container: {
        backgroundColor: '#eeeeee',
        marginBottom: 10
    }
});