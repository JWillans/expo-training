import React from 'react';
import type {
    CustomisationOptionsType,
    CustomisationPropertyType,
    ImageType, ProductPriceType,
    ProductType
} from "../../product/ProductType";
import {
    calculateProductPrice,
    createProductCustomisationDefaults,
    resolvePreviewImage,
    resolveProductPreviewImages
} from "../../product/Product";

import {View, Image, StyleSheet} from 'react-native';
import ProductPreview from "./ProductPreview";
import {formTypeMap} from "../form/FormType";
import ProductPrice from "./ProductPrice";

type ProductCustomiserProps = {
    product: ProductType
}

type ProductCustomiserState = {
    options: CustomisationOptionsType,
    price: ProductPriceType
}

const previewImageScale = 0.5;

export default class ProductCustomiser extends React.Component<ProductCustomiserProps, ProductCustomiserState>
{

    constructor(props)
    {
        super(props);

        const options = createProductCustomisationDefaults(this.props.product);

        this.state = {
            options: options,
            price: calculateProductPrice(this.props.product, options)
        }
    }

    createOnChangeOption(key: string): (value: any) => void
    {
        return (value: any) => {
            const options = {...this.state.options, [key]: value};
            this.setState({
                options: options,
                price: calculateProductPrice(this.props.product, options)
            });
        }
    }

    render()
    {
        const previewImages = resolveProductPreviewImages(this.props.product, this.state.options);

        return (
            <View style={styles.container}>

                <View style={styles.sticky}>

                    <View style={styles.previewColumn}>
                        <ProductPreview product={this.props.product} options={this.state.options} scale={0.25}/>
                    </View>

                    <View style={styles.priceColumn}>
                        <ProductPrice product={this.props.product} options={this.state.options} />
                    </View>

                </View>

                <View style={styles.form}>
                    {
                        this.props.product.customisation.properties.map((property: CustomisationPropertyType) => {
                            const FormType = formTypeMap[property.form.type].default;
                            const value = this.state.options[property.name] || null;
                            return <FormType
                                key={property.name}
                                options={property.form.options}
                                value={value}
                                onChange={this.createOnChangeOption(property.name)}
                            />
                        })
                    }
                </View>

            </View>
        )
    }

}

const styles = StyleSheet.create({
    container: {
        width: '100%'
    },
    sticky: {
        flexDirection: 'row',
        width: '100%',
        backgroundColor: '#eeeeee'
    },
    previewColumn: {
        flex: 0.5
    },
    priceColumn: {
        flex: 0.5,
        padding: 20
    },
    form: {
        width: '100%',
        paddingLeft: 20,
        paddingRight: 20
    }
});