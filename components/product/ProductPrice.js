import React from 'react';
import type {CustomisationOptionsType, ProductPriceElementType, ProductType} from "../../product/ProductType";
import {calculateProductPrice} from "../../product/Product";

import {View, Image, Text, StyleSheet} from 'react-native';

type ProductPriceProps = {
    product: ProductType,
    options: CustomisationOptionsType
}

function formatPrice(value: number, currency: string|undefined): string
{
    // @todo currency symbol;
    return `£${value.toFixed(2)}`;
}

export default class ProductPrice extends React.Component<ProductPriceProps>
{

    render()
    {

        const price = calculateProductPrice(this.props.product, this.props.options);

        return (
            <View style={styles.container}>

                {
                    price.elements.map((element: ProductPriceElementType) =>
                        <View style={styles.elementContainer} key={element.description}>
                            <Text style={styles.elementLabel}>{element.description}</Text>
                            <Text style={styles.elementValue}>{formatPrice(element.value)}</Text>
                        </View>
                    )
                }

                <View style={styles.totalContainer}>
                    <Text style={styles.totalLabel}>Total (excl. VAT)</Text>
                    <Text style={styles.totalValue}>{formatPrice(price.total_excl_vat)}</Text>
                </View>

            </View>
        )

    }

}

const styles = StyleSheet.create({
    container: {
    },
    elementContainer: {
        margin: 5
    },
    elementLabel: {
        fontWeight: 'bold'
    },
    elementValue: {

    },
    totalContainer: {
        margin: 5
    },
    totalLabel: {
        fontSize: 20,
        fontWeight: 'bold'
    },
    totalValue: {
        fontSize: 20
    }
});