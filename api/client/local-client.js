import {AbstractClient, RequestParameters, CacheOptions} from "./abstract-client";

const getDataMap = {
    '/product/1.json': require('../../data/product/1.json')
}

export default class LocalClient extends AbstractClient
{

    get(path: string, parameters: RequestParameters|undefined, cacheOptions: CacheOptions|undefined): Promise<{}>
    {
        return new Promise((resolve, reject) => {
            if (getDataMap[path] === undefined) {
                reject(`No mapping defined for ${path}`);
                return;
            }
            resolve(getDataMap[path]);
        });
    }

}