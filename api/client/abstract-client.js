export type RequestParameters = {
    [string]: string|number
}

export class CacheOptions
{

    ttl: number|null = null;

    expires: Date|null = null;

    constructor(ttl: number|null|undefined, expires: Date|null|undefined)
    {
        this.ttl = ttl||null;
        this.expires = expires||null;
    }

}

export class AbstractClient
{

    get(path: string, parameters: RequestParameters|undefined, cacheOptions: CacheOptions|undefined): Promise<{}>
    {
        return new Promise<{}>((resolve, reject) => {
            reject(`get() not implemented in ${this.constructor.name}`);
        })
    }

}