module.exports = function(api) {
  api.cache(true);
  return {
    presets: [
        'babel-preset-expo',
        '@babel/flow'
    ],
    plugins: [
      "transform-object-assign",
      "transform-class-properties",
      "syntax-dynamic-import"
    ]
  };
};
