import type {FormOptionsType} from "../components/form/FormType";

export type ConstraintType = {
    type: string,
    options: {
        [string]: any
    }
}

type FormType = {
    type: string,
    options: FormOptionsType
}

export type CustomisationPropertyType = {
    name: string,
    default_value: string|number|null,
    constraints: ConstraintType[],
    form: FormType
}

export type ImageType = {
    ref: string,
    offset: [number, number],
    size: [number, number]
}

export type CustomisationPreviewLayerType = {
    image: ImageType,
    condition: string|null
}

export type CustomisationPreviewType = {
    layers: CustomisationPreviewLayerType[]
}

export type ProductType = {
    id: number,
    name: string,
    customisation: {
        properties: CustomisationPropertyType[],
        preview: CustomisationPreviewType,
        pricing: {
            rules: CustomisationPricingRuleType[]
        }
    }
}

export type CustomisationPricingRuleType = {
    description: string,
    condition: string,
    value_expression: string
}

export type CustomisationOptionsType = {
    [string]: any
}

export type ConstraintViolationType = {
    propertyName: string,
    message: string
}

export type ProductPriceType = {
    elements: ProductPriceElementType[],
    total_excl_vat: number
}

export type ProductPriceElementType = {
    description: string,
    value: number
}