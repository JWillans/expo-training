import type {
    ConstraintViolationType,
    CustomisationPreviewLayerType,
    CustomisationPropertyType,
    ImageType,
    CustomisationOptionsType,
    ProductType,
    ProductPriceType, CustomisationPricingRuleType, ProductPriceElementType
} from "./ProductType";

// Images are loaded with here so that they can be referenced by name in config files and still be compiled into the app
const previewImageMap = {
    'base': require('../assets/images/products/layers/base.png'),

    'colours/cream': require('../assets/images/products/layers/colours/cream.png'),
    'colours/mint_green': require('../assets/images/products/layers/colours/mint_green.png'),
    'colours/parchment': require('../assets/images/products/layers/colours/parchment.png'),

    'wires/brown': require('../assets/images/products/layers/wires/brown.png'),
    'wires/copper': require('../assets/images/products/layers/wires/copper.png'),
    'wires/red': require('../assets/images/products/layers/wires/red.png'),

    'gold_bolts': require('../assets/images/products/layers/gold_bolts.png')
}

export function resolvePreviewImage(ref: string)
{
    return previewImageMap[ref];
}

function testLayerCondition(layer: CustomisationPreviewLayerType, product: ProductType, options: CustomisationOptionsType): boolean
{
    return eval(layer.condition); // @todo Yes, I know this is evil, I need to find a nice sandboxed expression engine
}

export function resolveProductPreviewImages(product: ProductType, options: CustomisationOptionsType): ImageType[]
{
    const layers = product.customisation.preview.layers;
    return layers
        .filter((layer: CustomisationPreviewLayerType) => {
            return testLayerCondition(layer, product, options);
        })
        .map((layer: CustomisationPreviewLayerType) => {
            return layer.image;
        })
    ;
}

export function createProductCustomisationDefaults(product: ProductType): CustomisationOptionsType
{
    return normaliseProductCustomisationOptions(product, {});
}

export function validateProductCustomisationOptions(product: ProductType, options: CustomisationOptionsType): ConstraintViolationType[]
{
    return []; // @todo
}

export function normaliseProductCustomisationOptions(product: ProductType, options: CustomisationOptionsType): CustomisationOptionsType
{
    const properties = product.customisation.properties || [];

    return properties.reduce((normalisedOptions: CustomisationOptionsType, property: CustomisationPropertyType) => {
        const value = options[property.name];
        normalisedOptions[property.name] = value === undefined ? property.default_value : value;
        return normalisedOptions;
    }, {});
}

function testCustomisationPricingRuleCondition(product: ProductType, options: CustomisationOptionsType, rule: CustomisationPricingRuleType): boolean
{
    return eval(rule.condition); // @todo Yes, I know this is evil, I need to find a nice sandboxed expression engine
}

function calculateCustomisationPricingRuleValue(product: ProductType, options: CustomisationOptionsType, rule: CustomisationPricingRuleType): number|null
{
    if(!testCustomisationPricingRuleCondition(product, options, rule)) return null;
    const value = eval(rule.value_expression);
    return parseFloat(value);
}

export function calculateProductPrice(product: ProductType, options: CustomisationOptionsType): ProductPriceType
{
    options = normaliseProductCustomisationOptions(product, options);

    const rules = product.customisation.pricing.rules;

    const elements = rules.reduce((elements: ProductPriceElementType[], rule: CustomisationPricingRuleType) => {
        const value = calculateCustomisationPricingRuleValue(product, options, rule);
        if(value === null) return elements;
        elements.push({description: rule.description, value: value})
        return elements;
    }, []);

    const totalExclVat = elements.reduce((total: number, element: ProductPriceElementType) => {
        total += element.value;
        return total;
    }, 0)

    return {
        elements: elements,
        total_excl_vat: totalExclVat
    }
}